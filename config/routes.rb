Rails.application.routes.draw do

  get 'admin/index'

  resources :categories do
    resources :proyects
  end

  resources :proyects do
    resources :pictures
  end
  
  devise_for :admins
  get 'home/index'

  root to:'admin#index'
end
